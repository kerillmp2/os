all:
	gcc -o lab_1 -pthread -Wall -Werror -Wpedantic main.c -lm

clean:
	rm -f lab_1
