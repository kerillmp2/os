#define _GNU_SOURCE_

#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <linux/futex.h>
#include <syscall.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>

#define A 353
#define B 0xEEE69520
#define D 25
#define E 195
#define I 36

typedef struct fill_memory_args {
	void* start;
	size_t size;
	FILE* random;
} fill_args;

void* fill_memory_from_thread( void* args ) {
	fill_args* input_args = ( fill_args* ) args;
	int c = fread( ( void* ) input_args->start, 1, input_args->size, input_args->random );

	if( c <= 0 ){
		perror( "Can't fill memory with data!" );
		exit(1);
	}

	return 0;
}

void fill_memory( void* begin, size_t size ) {
	FILE* random = fopen( "/dev/urandom", "r" );
	pthread_t threads[D];

	begin = mmap( begin, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);

	if( begin == MAP_FAILED ){
		perror("Can't map data in memory!");
		exit(1);
	}

	fill_args args = { begin, size, random };

	for( int i = 0; i < D; ) {
		pthread_create(&threads[i], NULL, fill_memory_from_thread, (void *) &args);
	}

	for( int i = 0; i < D; ) {
		pthread_join(threads[i], NULL);
	}

	fclose( random );
	munmap( begin, size );
}

_Noreturn void* write_thread( void* futex ) {

	FILE* random;
	int value;
	int check_random = 0;
	int out = open( "output", O_CREAT | O_WRONLY | O_TRUNC, 0700 );
	if( out == -1 ){
		perror( "Can't open file" );
		exit( 1 );
	}

	while ( 1 ) {
		random = fopen( "/dev/urandom", "r" );
		check_random = 0;
		while ( !check_random ) {
			syscall( SYS_futex, ( int* ) futex, FUTEX_WAIT, 1, NULL, NULL, 0 );
			*( ( int* ) futex ) = 1;
            		if ( fread( &value, sizeof( int ) - 1, 1, random ) < 0) {
				check_random = 1;
				printf("Random file is empty");
			} else {
				value = abs( value );
				size_t input_size = floor( log10( value ) + 2 );
				char val_str[input_size];
				sprintf( val_str, "%d ", value );
				if( write( out, val_str, input_size ) <= 0 ) {
					perror( "Can't write value into file" );
					exit( 1 );
				}
			}
			*( ( int* ) futex) = 0;
			syscall( SYS_futex , ( int* ) futex, FUTEX_WAKE, 1, NULL, NULL, 0 );
		}
		fclose( random );
	}
	close( out );
}

_Noreturn void* find_max( void *futex ) {
	while( 1 ) {
		int maximum = INT_MIN;
		int value = INT_MIN;
		FILE *f = fopen("output", "r");
		if(f == NULL){
			continue;
		}
		syscall( SYS_futex, ( int* ) futex, FUTEX_WAIT, 1, NULL, NULL, 0 );
		*( ( int* ) futex ) = 1;

		while( fscanf( f, "%d ", &value ) > 0 ) {
			maximum = fmax( maximum, value );
		}
		printf( "Max number is: %d\n", maximum );

		*( ( int* ) futex ) = 0;
		syscall( SYS_futex, ( int* ) futex, FUTEX_WAKE, 1, NULL, NULL, 0 );
		fclose(f);
	}
}

void fill_file( long long size ) {
	int futex_temp = 1;
	int *futex = &futex_temp;
	pthread_t w_thread;
	pthread_t stat_threads[I];

	pthread_create( &w_thread, NULL, write_thread, ( void* ) futex );
	*futex = 0;

	for( int i = 0; i < I; ++i ) {
		pthread_create( &stat_threads[i], NULL, find_max, ( void* ) futex );
	}

	pthread_join( w_thread, NULL );

	for( int i = 0; i < I; i++ ) {
		pthread_join( stat_threads[i], NULL );
	}
}

int main() {
	fill_memory( ( void* ) B, A * 1024 * 1024 );
	fill_file( E * 1024 * 1024 );
	return 0;
}
